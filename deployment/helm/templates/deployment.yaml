apiVersion: apps/v1
kind: Deployment
metadata:
  name: "{{ template "app.name" . }}"
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "app.labels" . | nindent 4 }}
    app.kubernetes.io/instance: {{ include "app.revision" . }}
    app.kubernetes.io/part-of: rse
spec:
  replicas: {{ .Values.replicaCount }}
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 0
  selector:
    matchLabels:
      {{- include "app.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "app.labels" . | nindent 8 }}
      annotations:
        {{- include "app.metricsAnnotations" . | nindent 8 }}
{{- if .Values.podAnnotations }}
{{ toYaml .Values.podAnnotations | indent 8 }}
{{- end }}
    spec:
      securityContext:
{{- include "app.securitycontext" . | nindent 8 }}
      imagePullSecrets:
        - name: {{ .Values.image.pullSecrets }}
      containers:
      - name: {{ template "app.name" . }}
        image: "{{ .Values.image.repository }}/{{ .Values.image.name }}:{{ default .Chart.AppVersion .Values.image.tag }}"
        imagePullPolicy: {{ .Values.image.pullPolicy | quote }}
        env:
          - name: LOG_LEVEL
            value: {{ .Values.log.level | default "INFO" }}
          - name: LOG_ENCODING
            value: {{ .Values.log.encoding | default "json" }}
          - name: HTTP_HOST
            value: {{ .Values.signer.http.host | quote }}
          - name: HTTP_PORT
            value: {{ .Values.signer.http.port | quote }}
          - name: HTTP_IDLE_TIMEOUT
            value: {{ .Values.signer.http.timeout.idle | quote }}
          - name: HTTP_READ_TIMEOUT
            value: {{ .Values.signer.http.timeout.read | quote }}
          - name: HTTP_WRITE_TIMEOUT
            value: {{ .Values.signer.http.timeout.write | quote }}
          - name: VAULT_ADDR
            value: {{ .Values.signer.vault.addr | quote }}
          - name: VAULT_TOKEN
            value: {{ .Values.signer.vault.token | quote }}
          - name: VAULT_SIGNING_KEY
            value: {{ .Values.signer.vault.key.signing | quote }}
          - name: VAULT_SUPPORTED_KEYS
            value: {{ .Values.signer.vault.key.supported | quote }}
          - name: CREDENTIAL_ISSUER
            value: {{ .Values.signer.credential.issuer | quote }}
{{- if .Values.extraVars }}
{{ toYaml .Values.extraVars | indent 8 }}
{{- end }}
        ports:
        {{- if .Values.metrics.enabled }}
          - name: monitoring
            containerPort: {{ .Values.metrics.port }}
        {{- end }}
          - name: http
            containerPort: {{ .Values.signer.http.port }}
        readinessProbe:
          httpGet:
            path: /readiness
            port: {{ .Values.signer.http.port }}
          initialDelaySeconds: 5
          periodSeconds: 5
          successThreshold: 2
          failureThreshold: 2
          timeoutSeconds: 5
        resources:
{{ toYaml .Values.resources | indent 10 }}
